/*
 *File: nfa_to_dfa.h
 *Creator: Muskaan Mendiratta
 *Created: Fri Sep 21 2018
 */

#include "nfa.h"
#include "dfa.h"

extern DFA NFA_to_DFA(NFA nfa);
