/*
 * File: dfa.c
 *Creator: Muskaan Mendiratta
 *Created Wed Sep 11 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dfa.h"

typedef struct Node *Node;

#define TRUE 1
#define FALSE 0

/*
 *Node struct
 */
struct Node{
  int value;
  int next_state;
};
//todo: dlete sjd

//todo: delete this comment

/*
 *make DFA struct
 */
struct DFA {
  int nstates;
  //set of accepted characters
  //define next state, and if the state is final or not
  int current_state;
  int accepting_state;
  Node** transition_table;
  
};

Node new_DFA_Node (int value){
  Node this = (Node)malloc(sizeof(struct Node));
  if (this == NULL){
    return NULL; //Out of memory
  }
  this->value = value;
  this->next_state = 0;
  return this;
}
    
DFA new_DFA(int nstates){
  DFA this = (DFA)malloc(sizeof(struct DFA));
  if (this == NULL){
    return NULL; //Out of memory
  }
  this->nstates = nstates;

  /*transition table
   * number of columns = number of states
   * number of rows = number of ASCII characters = 128
   */
  this->transition_table = (Node**)malloc(this->nstates*sizeof(Node*));
  for (int i=0; i<this->nstates;i++){
    this->transition_table[i] = (Node*)malloc(128*sizeof(struct Node));
  }
  //  int current_state = 0;
  return this;
  
}

extern void DFA_free(DFA dfa){
  //free dfa, remove from memory
  free(dfa);
}

extern int DFA_get_size(DFA dfa){
 return dfa->nstates;
}

extern int DFA_get_transition(DFA dfa, int src, char sym){
 
  //index of src = [state][symbol]
  int sym_int = sym;    //ASCII value of the character
  
  return dfa->transition_table[src][sym_int]->next_state;    
  
}

extern void DFA_set_transition(DFA dfa, int src, char sym, int dst){
  int sym_int = sym;
  Node node = new_DFA_Node(sym_int);
  //TODO: boolean should be true if accepting state
  dfa->transition_table[src][sym_int] = node;
  node->next_state = dst;
}

extern void DFA_set_transition_str(DFA dfa, int src, char *str, int dst){
  //TODO
}

extern void DFA_set_transition_all(DFA dfa, int src,int dst){
     for (int j=0; j<128;j++){
      if (dfa->transition_table[src][j] == NULL){
	Node node = new_DFA_Node(src);
	dfa->transition_table[src][j] = node;
	node->next_state = dst;
      }
      
  }
}

//TODO: REMOVE THIS SHIT
extern bool test(bool b){
  return b;  
}
  
extern void DFA_set_accepting(DFA dfa, int state, bool value){
  dfa->accepting_state = state;
}

extern bool DFA_get_accepting(DFA dfa, int state){
  if (dfa->accepting_state == state){
    return TRUE;
  }else{
    return FALSE;         
  }
}


extern bool DFA_execute(DFA dfa, char *input){
  int i = 0;
  while (input[i]!='\0'){
    int i_c = input[i];
    if (dfa->transition_table[dfa->current_state][i_c] == NULL){
      printf("%s\n", "Pattern did not match");
	return 0;
      }
    dfa->current_state = DFA_get_transition(dfa,dfa->current_state, input[i]);    
    DFA_print(dfa, input[i]);
    i+=1;
  }
  if (dfa->accepting_state == dfa->current_state){
    printf("%s\n", "Pattern matched!");
  }else{
    printf("%s\n", "Pattern did not match");
  }
  return (dfa->accepting_state == dfa->current_state);
}


extern void DFA_print(DFA dfa, char c){
  printf("------%c------>( %d )", c, dfa->current_state);
}

