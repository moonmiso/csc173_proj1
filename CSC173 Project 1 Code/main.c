/*
 *File: main.c
 *Creator: Muskaan Mendiratta
 *Created: Fri Sep 21 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include "dfa.h"
#include "nfa.h"
#include "nfa_to_dfa.h"

extern void dfa1(){
  DFA dfa1 = new_DFA(4);
    char z = 'z';
    char x = 'x';
    char y = 'y';

    //set transitions
    DFA_set_transition(dfa1, 0, z, 1);
    DFA_set_transition_all(dfa1, 0, 0);
    DFA_set_transition(dfa1, 1, x, 2);
    DFA_set_transition(dfa1, 2, y, 3);
    DFA_set_accepting(dfa1, 3, 1);
    
    printf("%s", "Enter a string to test");
    char string1[256];
    scanf("%s",string1);
    printf("String entered: %s\n", string1);
    printf("%d\n", DFA_execute(dfa1, string1));
   
    DFA_free(dfa1);
}

extern void dfa2(){
  DFA dfa2 = new_DFA(4);
    char a = 'a';
    char b = 'b';
    char c = 'c';

    //set transitions
    DFA_set_transition(dfa2, 0, a, 1);
    DFA_set_transition(dfa2, 1, b, 2);
    DFA_set_transition(dfa2, 2, c, 3);
    DFA_set_transition_all(dfa2, 3, 3);
    DFA_set_accepting(dfa2, 3, 1);
    
    printf("%s", "Enter a string to test");
    char string2[256];
    scanf("%s",string2);
    printf("String entered: %s\n", string2);
    printf("%d\n", DFA_execute(dfa2, string2));
    
    DFA_free(dfa2);
}

extern void dfa3(){
   DFA dfa3 = new_DFA(2);
    DFA_set_transition(dfa3, 0, '1', 0);
    DFA_set_transition(dfa3, 0, '0', 1);
    DFA_set_transition(dfa3, 1, '0', 0);
    DFA_set_transition(dfa3, 1, '1', 1);
    DFA_set_accepting(dfa3, 1, 1);

    printf("%s", "Enter a string to test");
    char string3[256];
    scanf("%s",string3);
    printf("String entered: %s\n", string3);
    printf("%d\n", DFA_execute(dfa3, string3));
    
    DFA_free(dfa3);
}

extern void dfa4(){
  DFA dfa4 = new_DFA(4);
     DFA_set_transition(dfa4, 0, '0', 1);
     DFA_set_transition(dfa4, 0, '1', 3);
     DFA_set_transition(dfa4, 1, '0', 0);
     DFA_set_transition(dfa4, 1, '1', 2);
     DFA_set_transition(dfa4, 2, '0', 3);
     DFA_set_transition(dfa4, 2, '1', 1);
     DFA_set_transition(dfa4, 3, '0', 2);
     DFA_set_transition(dfa4, 3, '1', 0);
     DFA_set_accepting(dfa4, 2, 1);

     printf("%s", "Enter a string to test");
    char string4[256];
    scanf("%s",string4);
    printf("String entered: %s\n", string4);
    printf("%d\n", DFA_execute(dfa4, string4));
    
     DFA_free(dfa4);
}
  
extern void dfa5(){
  DFA dfa5 = new_DFA(6);
  

    //set transitions
    DFA_set_transition(dfa5, 0, 'h', 1);
    DFA_set_transition(dfa5, 1, 'e', 2);
    DFA_set_transition(dfa5, 2, 'l', 3);
    DFA_set_transition(dfa5, 3, 'l', 4);
    DFA_set_transition(dfa5, 4, 'o', 5);
    DFA_set_transition_all(dfa5, 5, 5);
    DFA_set_accepting(dfa5, 5, 1);
    
    printf("%s", "Enter a string to test");
    char string5[256];
    scanf("%s",string5);
    printf("String entered: %s\n", string5);
    printf("%d\n", DFA_execute(dfa5, string5));
    
    DFA_free(dfa5);
}
  

extern void execute_dfa(){
  printf("%s\n", "Part 1 -- Deterministic Finite Automata");
  printf("%s\n", "1. Exactly the string 'zxy'");
  printf("%s\n", "2. Any string that starts with the characters abc");
  printf("%s\n", "3. Binary input with an odd number of 0's");
  printf("%s\n", "4. Binary input with an odd number of both 0's and 1's");
  printf("%s\n", "5. Muskaan's pattern: is greeting? (Identifies hello at the start)");
  printf("%s\n", "Enter your choice (1/2/3/4/5)");

  char ch;
  scanf("%c%*c", &ch);
  if (ch == '1'){
    dfa1();
    
  }
  else if (ch == '2'){
    dfa2();
  }
  else if (ch == '3'){
    dfa3();
  }
  else if (ch == '4'){
    dfa4();
  }
  else if (ch == '5'){
    dfa5();
  }
  
}

extern void nfa1(){
  
    NFA nfa1 = new_NFA(4);
    NFA_set_accepting(nfa1, 3);
    NFA_add_transition_all(nfa1, 0, 0);
    NFA_add_transition(nfa1, 0, 'c', 0);
    NFA_add_transition(nfa1, 0, 'c', 1);
    NFA_add_transition(nfa1, 1, 'a', 2);
    NFA_add_transition(nfa1, 2, 'r', 3); 
    
    char string1[256];
    printf("%s\n", "Enter a string to test");
    scanf("%s", string1);
    printf("Scanned string: %s\n", string1);
    printf("%d\n", NFA_execute(nfa1, string1));
}

extern void nfa2(){
   NFA nfa2 = new_NFA(4);
    NFA_set_accepting(nfa2, 3);
    NFA_add_transition_all(nfa2, 0, 0);
    NFA_add_transition(nfa2, 0, 'c', 1);
    NFA_add_transition(nfa2, 0, 'a', 2);
    NFA_add_transition(nfa2, 2, 'r', 3);
    NFA_add_transition_all(nfa2, 3, 3);

    char string2[256];
    printf("%s\n", "Enter a string to test");
    scanf("%s", string2);
    printf("Scanned string: %s\n", string2);
    printf("%d\n", NFA_execute(nfa2, string2));
}

extern void nfa3(){
  NFA nfa3 = new_NFA(20);
    NFA_add_transition_all(nfa3, 0, 0);
    NFA_add_transition(nfa3, 0, 'a', 1);
    NFA_add_transition_all(nfa3, 1, 1);
    NFA_add_transition(nfa3, 1, 'a', 2);
    NFA_add_transition_all(nfa3, 2, 2);
    NFA_add_transition(nfa3, 0, 'g', 3);
    NFA_add_transition_all(nfa3, 3, 3);
    NFA_add_transition(nfa3, 3, 'g', 4);
    NFA_add_transition_all(nfa3, 4, 4);
    NFA_add_transition(nfa3, 0, 'h', 5);
    NFA_add_transition_all(nfa3, 5, 5);
    NFA_add_transition(nfa3, 5, 'h', 6);
    NFA_add_transition_all(nfa3, 6, 6);
    NFA_add_transition(nfa3, 0, 'i', 7);
    NFA_add_transition_all(nfa3, 7, 7);
    NFA_add_transition(nfa3, 7, 'i', 8);
    NFA_add_transition_all(nfa3, 8, 8);
    NFA_add_transition(nfa3, 0, 'n', 9);
    NFA_add_transition_all(nfa3, 9, 9);
    NFA_add_transition(nfa3, 9, 'n', 10);
    NFA_add_transition_all(nfa3, 10, 10);
    NFA_add_transition(nfa3, 10, 'n', 11);
    NFA_add_transition_all(nfa3, 11, 11);
    NFA_add_transition(nfa3, 0, 'o', 12);
    NFA_add_transition_all(nfa3, 12, 12);
    NFA_add_transition(nfa3, 12, 'o', 13);
    NFA_add_transition_all(nfa3, 13, 13);
    NFA_add_transition(nfa3, 0, 's', 14);
    NFA_add_transition_all(nfa3, 14, 14);
    NFA_add_transition(nfa3, 14, 's', 15);
    NFA_add_transition_all(nfa3, 15, 15);
    NFA_add_transition(nfa3, 0, 't', 16);
    NFA_add_transition_all(nfa3, 16, 16);
    NFA_add_transition(nfa3, 16, 't', 17);
    NFA_add_transition_all(nfa3, 17, 17);
    NFA_add_transition(nfa3, 0, 'w', 18);
    NFA_add_transition_all(nfa3, 18, 18);
    NFA_add_transition(nfa3, 18, 'w', 19);
    NFA_add_transition_all(nfa3, 19, 19);
    
    NFA_set_accepting(nfa3, 2);
    NFA_set_accepting(nfa3, 4);
    NFA_set_accepting(nfa3, 6);
    NFA_set_accepting(nfa3, 8);
    NFA_set_accepting(nfa3, 11);
    NFA_set_accepting(nfa3, 13);
    NFA_set_accepting(nfa3, 15);
    NFA_set_accepting(nfa3, 17);
    NFA_set_accepting(nfa3, 19);

    char string3[256];
    printf("%s\n", "Enter a string to test");
    scanf("%s", string3);
    printf("Scanned string: %s\n", string3);
    printf("%d\n", NFA_execute(nfa3, string3));
}

extern void nfa4(){
   NFA nfa4 = new_NFA(4);
    NFA_set_accepting(nfa4, 3);
    NFA_add_transition_all(nfa4, 0, 0);
    NFA_add_transition(nfa4, 0, 'l', 1);
    NFA_add_transition(nfa4, 0, 'o', 2);
    NFA_add_transition(nfa4, 2, 'l', 3);
    NFA_add_transition_all(nfa4, 3, 3);

    char string4[256];
    printf("%s\n", "Enter a string to test");
    scanf("%s", string4);
    printf("Scanned string: %s\n", string4);
    printf("%d\n", NFA_execute(nfa4, string4));
}



extern void execute_nfa(){
  printf("%s\n", "Part 2 -- Non-Deterministic Finite Automata");
  printf("%s\n", "1. Strings ending in 'car' ");
  printf("%s\n", "2. Strings containing 'car' ");
  printf("%s\n", "3. Partial anagrams of Washington");
  printf("%s\n", "4. Muskaan's pattern (checks to see if contains lol) lol");
  printf("%s\n", "Enter your choice (1/2/3/4)");
  char ch2;
  scanf("%c%*c", &ch2);
  if (ch2 == '1'){
    nfa1();
  }
  else if (ch2 == '2'){
    nfa2();
  }
  else if (ch2 == '3'){
    nfa3();
    
  }
  else if (ch2 == '4'){
    nfa4();
  }    
}

extern void execute_nfa_dfa(){
  printf("%s\n", "Part 3 -- Non-Deterministic Finite Automata to Finite Deterministic Automata");
  printf("%s\n", "1. Strings ending in 'car' ");
  printf("%s\n", "2. Strings containing 'car' ");
  char ch3;
  printf("%s\n", "Enter your choice 1/2");
  scanf("%c%*c", &ch3);

  if (ch3 == '1'){
    
    NFA nfa1 = new_NFA(4);
    NFA_set_accepting(nfa1, 3);
    NFA_add_transition_all(nfa1, 0, 0);
    NFA_add_transition(nfa1, 0, 'c', 0);
    NFA_add_transition(nfa1, 0, 'c', 1);
    NFA_add_transition(nfa1, 1, 'a', 2);
    NFA_add_transition(nfa1, 2, 'r', 3); 

    char string1[256];
    printf("%s\n", "Enter a string to test");
    scanf("%s", string1);
    printf("Scanned string: %s\n", string1);
    //    printf("%d\n", NFA_execute(nfa1, string1));

    printf("%s\n", "-----------------------------");

    DFA dfa_nfa1 = NFA_to_DFA(nfa1);
    printf("%d\n", DFA_execute(dfa_nfa1, string1));
  }
  else if (ch3 == '2'){
   
    NFA nfa2 = new_NFA(4);
    NFA_set_accepting(nfa2, 3);
    NFA_add_transition_all(nfa2, 0, 0);
    NFA_add_transition(nfa2, 0, 'c', 1);
    NFA_add_transition(nfa2, 0, 'a', 2);
    NFA_add_transition(nfa2, 2, 'r', 3);
    NFA_add_transition_all(nfa2, 3, 3);

    char string2[256];
    printf("%s\n", "Enter a string to test");
    scanf("%s", string2);
    printf("Scanned string: %s\n", string2);

    printf("%d\n", NFA_execute(nfa2, string2));

    printf("%s\n", "-----------------------------");

    DFA dfa_nfa2 = NFA_to_DFA(nfa2);
    printf("%d\n", DFA_execute(dfa_nfa2, string2));
  }
    
}
    
  
  
  
  
int main(){
  
  char p;
  while (p != '0'){
    
    printf("%s\n", "Finite Automata Machine");
    printf("%s\n", "---------------------------------------------------------------------");
    printf("%s\n", "1. Deterministic Finite Automata");
    printf("%s\n", "2. Non-Deterministic Finite Automata");
    printf("%s\n", "3. Converting Deterministic Finite Automata to Non-Deterministic Finite Automata");
    printf("%s\n", "Enter 1/2/3 or 0 to quit. If you just came from an automaton implementation to the main menu, PLEASE PRESS ENTER BEFORE ENTERING ANOTHER NUMBER");
   
    scanf("%c%*c",&p);
    
    
    if(p == '1'){
      execute_dfa();
    }
    else if(p == '2'){
      execute_nfa();
    }
    else if(p == '3'){
      execute_nfa_dfa();
    }
    else if (p == '0'){
      break;
    }

  }
}




















    
    
    
