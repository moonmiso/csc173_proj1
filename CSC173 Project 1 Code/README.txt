CSC173_Proj1
Name: Muskaan Mendiratta
Assignment Number: Project 01, Unit 01
I did not collaborate with anyone on this project.

Files:
dfa.h		Header file 
dfa.c		Contains data structure implementation for a DFA
nfa.h		Header file
nfa.c		Contains data structure implementation for an NFA
nfa_to_dfa.h	Header file
nfa_to_dfa.c	Contains a method that converts NFA to DFA using subset construction method
IntHashSet.h
IntHashSet.c	Taken from Professor Ferguson

auto		Executable file
Makefile	To compile


Compiling and executing instructions:
I have included everything in my Makefile.
Steps for compiling and executing:

make
./auto

On the console:
I have a minor bug in my REPl, which makes you enter after you get back to the main menu. It is included in the print statement:

Finite Automata Machine
---------------------------------------------------------------------
1. Deterministic Finite Automata
2. Non-Deterministic Finite Automata
3. Converting Deterministic Finite Automata to Non-Deterministic Finite Automata
Enter 1/2/3 or 0 to quit. If you just came from an automaton implementation to the main menu, PLEASE PRESS ENTER BEFORE ENTERING ANOTHER NUMBER.

Project submission PDF:
I used -std=c99 -Wall -Werror
I used valgrind but did not get a clean report
(My PDF editor didn't let me check just one box, and checked all boxes)
